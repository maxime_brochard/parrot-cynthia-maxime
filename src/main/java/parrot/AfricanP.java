package parrot;

public class AfricanP extends Parrot {

    public AfricanP(int coco, double vol, boolean nail){
        super(coco, vol, nail);

    }

    @Override
    public double getSpeed() {
        return Math.max(0, getBaseSpeed() - getLoadFactor() * this.numberOfCoconuts);
    }


}
