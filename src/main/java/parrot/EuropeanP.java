package parrot;
public class EuropeanP extends Parrot{

    public EuropeanP(int coco, double vol, boolean nail) {
        super(coco, vol, nail);

    }

    @Override
    public double getSpeed() {
        return getBaseSpeed();
    }
}

