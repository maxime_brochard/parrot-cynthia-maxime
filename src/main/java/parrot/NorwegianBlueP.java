package parrot;
public class NorwegianBlueP extends Parrot {

    public NorwegianBlueP(int coco, double vol, boolean nail){
        super(coco, vol, nail);

    }

    @Override
    public double getSpeed() {
        return (isNailed) ? 0 : getBaseSpeed(voltage);
    }
}
